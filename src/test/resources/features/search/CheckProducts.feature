Feature: Check the product search endpoint

  Background:
    Given the user calls the product search endpoint


  @NegativeScenario
  Scenario Outline: User searches data related to products with no records as "orange" or "apple"
    Given the user calls the given endpoint with the route <product>
    And the status code is <statuscode>
    When the user receives an empty list of items for the current product
    Then the results should be empty
    Examples:
      | product | statuscode |
      | orange  | 200        |
      | apple   | 200        |

  @PositiveScenario
  Scenario Outline: User searches data related to products with existing records as "cola" or "pasta"
    Given the user calls the given endpoint with the route <product>
    And the status code is <statuscode>
    When the user sees the results displayed for the current product
    Then the results should be not empty
    And the results should contain data about <product>
    Examples:
      | product | statuscode |
      | cola    | 200        |
      | pasta   | 200        |

  @PositiveScenario
  Scenario Outline: User checks the data related to products with existing records as "cola" or "pasta"
    Given the user calls the given endpoint with the route <product>
    And the status code is <statuscode>
    And the results contain valid data about the product
    Examples:
      | product | statuscode |
      | cola    | 200        |
      | pasta   | 200        |

  @NegativeScenario
  Scenario Outline: User searches data related to non-existing products
    Given the user calls the given endpoint with the route <product>
    And the status code is <statuscode>
    Then the error message contains the requested_item as <product>
    Then the user can see an error message
    And the error message has fields with valid formatting
    Examples:
      | product | statuscode |
      | car     | 404        |
      | mango   | 404        |

  @SecurityTest
  Scenario Outline: User searches data for products with invalid names
    Given the user calls the given endpoint with the route <product>
    Then the user receives a generic error
    And the status code is <statuscode>
    Examples:
      | product  | statuscode |
      | /!,.$£:  | 404        |
      | /;:?>=-  | 404        |
      | ../../.. | 404        |

  @SecurityTest
  Scenario Outline: User searches for security breaches trying to call endpoint without any route
    Given the user calls the given endpoint with no route defined
    Then the user receives error <errormessage> failing to get unauthorized access to resources
    And the status code is <statuscode>
    Examples:
      | statuscode | errormessage      |
      | 401        | Not authenticated |