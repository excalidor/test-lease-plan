package starter.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Model class
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Product {
    private String provider;
    private String title;
    private String url;
    private String brand;
    private double price;
    private String unit;
    private boolean isPromo;
    private String promoDetails;
    private String image;

    public void printProduct() {
        System.out.println("provider: " + getProvider());
        System.out.println("title: " + getTitle());
        System.out.println("url: " + getUrl());
        System.out.println("brand: " + getBrand());
        System.out.println("price: " + getPrice());
        System.out.println("unit: " + getUnit());
        System.out.println("isPromo: " + isPromo());
        System.out.println("promoDetails: " + getPromoDetails());
        System.out.println("image: " + getImage());
    }
}
