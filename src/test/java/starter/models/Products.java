package starter.models;

import lombok.Data;

import java.util.List;

/**
 * Model class
 */
@Data
public class Products {
    private List<Product> products;
}
