package starter.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Model class
 */
@Data
public class ErrorDetails {

    private boolean error;
    private String message;
    @JsonProperty("requested_item")
    private String requestedItem;
    @JsonProperty("served_by")
    private String servedBy;
}
