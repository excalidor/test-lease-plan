package starter.models;

import lombok.Data;

/**
 * Model class
 */
@Data
public class ErrorHolder {

    private ErrorDetails detail;
}
