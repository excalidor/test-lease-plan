package starter.api;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import starter.stepdefinitions.SearchStepDefinitions;

import java.util.Properties;

import static starter.Utils.PropertiesLoader.loadProperties;

/**
 * ApiLayer class - holds the variables and methods for handling the API requests
 * This class can be extended with various other methods for API calls
 */
public class ApiLayer {

    protected Response response;
    protected Properties properties = loadProperties();
    protected static final Logger LOGGER = LoggerFactory.getLogger(SearchStepDefinitions.class);

    /**
     * Wrapper for GET method from Serenity Rest
     *
     * @param url    - the url for request
     * @param params - list of query params for get (optional)
     */
    protected void get(String url, Object... params) {
        LOGGER.info("URL: " + url);
        if (properties.getProperty("requestLogging").equals("true")) {
            response = SerenityRest.given().log().all().get(url, params).then().log().all().and().extract().response();
        } else {
            response = SerenityRest.given().get(url, params);
        }
        if (properties.getProperty("logProducts").equals("true")) {
            LOGGER.info(response.asString());
        }
    }
}
