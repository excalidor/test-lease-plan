package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import starter.models.ErrorHolder;
import starter.models.Product;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import starter.models.Products;
import starter.stepImplementations.SearchStepImplementations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;


import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static starter.Utils.Stringz.*;

/**
 * Step definitions class - used by Cucumber+Serenity
 */
public class SearchStepDefinitions extends SearchStepImplementations {

    @Steps
    protected Products products;
    protected ErrorHolder errorHolder;
    private static final Logger LOGGER = LoggerFactory.getLogger(SearchStepDefinitions.class);

    @Then("^the error message contains the requested_item as ([^\"]*)$")
    public void theErrorMessageContainsTheRequestedItemAs(String product) {
        LOGGER.info(ERROR_MESSAGE_CONTENT_CHECK);
        Assert.assertEquals(errorHolder.getDetail().getRequestedItem(), product);
    }

    @When("^the user calls the given endpoint with the route ([^\"]*)$")
    public void theUserCallsTheGivenEndpointWithTheRoute(String route) {
        get(GET_PRODUCTS_ENDPOINT + "/" + route);
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            if (!response.path("detail.error").toString().contains("true")) {
                products.setProducts(objectMapper.readValue(response.asString(), new TypeReference<List<Product>>() {
                }));
                logProducts(products);
            } else {
                ObjectMapper objMapper = new ObjectMapper();
                errorHolder = objMapper.readValue(response.asString(), ErrorHolder.class);
                LOGGER.warn("Specific error message received when requesting the endpoint.");
            }
        } catch (Exception e) {
            if (properties.getProperty("requestLogging").equals("true")) {
                LOGGER.error("Generic error when requesting the endpoint.");
                e.printStackTrace();
            }
        }
    }

    @Then("the user sees the results displayed for the current product$")
    public void theUserSeesTheResultsDisplayedForTheCurrentProduct() {
        LOGGER.info(PRODUCT_LIST_SIZE_CHECK);
        Assert.assertFalse(PRODUCTS_NOT_PRESENT, products.getProducts().isEmpty());
    }

    @Given("the user calls the product search endpoint")
    public void theUserCallsTheProductSearchEndpoint() {
        LOGGER.info(URL_FORMAT_CHECK);
        Assert.assertTrue(URL_IS_NOT_VALID, GET_PRODUCTS_ENDPOINT.matches(URL_REGEX_MATCH));
        LOGGER.info(CALLING_THE_ENDPOINT + GET_PRODUCTS_ENDPOINT);
    }

    @Then("the results should be not empty")
    public void theResultsShouldBeNotEmpty() {
        LOGGER.info(EXPECTED_NOT_EMPTY_LIST);
        Assert.assertNotEquals(RESULT_IS_EMPTY, "[]", Collections.singletonList(response.asString()).get(0));
    }

    @Then("the results should be empty")
    public void theResultsShouldBeEmpty() {
        LOGGER.info(EXPECTED_EMPTY_LIST);
        Assert.assertEquals(RESULT_NOT_EMPTY, "[]", Collections.singletonList(response.asString()).get(0));
    }

    @Then("^the results should contain data about ([^\"]*)$")
    public void theResultsShouldContainDataAboutProduct(String prod) {
        LOGGER.info(CHECK_TITLE_FIELD + prod);
        boolean check = false;
        for (Product product : products.getProducts()) {
            if (product.getTitle().toLowerCase().contains(prod.toLowerCase())) {
                check = true;
                break;
            }
        }
        Assert.assertTrue(NO_REFERENCE_TO_PRODUCT + prod, check);
    }

    @Then("^the status code is (\\d+)$")
    public void theStatusCodeIsStatuscode(int statuscode) {
        LOGGER.info(STATUS_CODE_CHECKING);
        restAssuredThat(response -> response.statusCode(statuscode));
    }

    @Then("the user can see an error message")
    public void theUserCanSeeAnErrorMessage() {
        LOGGER.info(ERROR_MESSAGE_EXPECTED);
        Assert.assertTrue(NO_ERROR_MESSAGE, response.path("detail.error").toString().contains("true"));
    }

    @When("the user receives an empty list of items for the current product$")
    public void theUserReceivesAnEmptyIstOfItemsForTheProductProduct() {
        LOGGER.info(EMPTY_LIST_EXPECTED);
        Assert.assertEquals(PRODUCTS_ARE_PRESENT, 0, products.getProducts().size());
    }

    @Then("the user receives a generic error")
    public void theUserReceivesAGenericError() {
        LOGGER.info(GENERIC_ERROR_RECEIVED);
        Assert.assertEquals(GENERIC_ERROR_NOT_PRESENT, "Not Found", response.path("detail"));
    }

    @Then("the results contain valid data about the product$")
    public void theResultsContainValidDataAboutTheProduct() {
        LOGGER.info(CHECKING_PRODUCT_DATA_VALIDITY);
        for (Product product : products.getProducts()) {
            Assert.assertTrue(IMAGE_URL_ERROR, product.getImage().matches(URL_REGEX_MATCH));
            Assert.assertTrue(TITLE_ERROR, product.getTitle().matches(GENERIC_STRING_REGEX));
            Assert.assertTrue(IS_PROMO_ERROR, (String.valueOf(product.isPromo())).matches(BOOLEAN_REGEX_MATCH));
            Assert.assertTrue(PRICE_ERROR, Double.toString(product.getPrice()).matches(DOUBLE_REGEEX_MATCH) && product.getPrice() > 0);
            Assert.assertTrue(BRAND_ERROR, product.getBrand().matches(GENERIC_STRING_REGEX));
            Assert.assertTrue(PROMO_DETAILS__ERROR, product.getPromoDetails().matches(GENERIC_STRING_REGEX) || product.getPromoDetails().matches(BOOLEAN_REGEX_MATCH));
            Assert.assertTrue(PROVIDER_ERROR, product.getProvider().matches(GENERIC_STRING_REGEX));
            Assert.assertTrue(UNIT_ERROR, product.getUnit().matches(GENERIC_STRING_REGEX));
            Assert.assertTrue(URL_IS_NOT_VALID, product.getUrl().matches(URL_REGEX_MATCH));
        }
    }

    @Then("the error message has fields with valid formatting")
    public void theErrorMessageHasFieldsWithValidFormatting() {
        LOGGER.info(CHECKING_ERROR_FIELDS_VALIDITY + errorHolder.getDetail().getMessage());
        Assert.assertTrue(IS_ERROR_NOT_BOOLEAN, String.valueOf(errorHolder.getDetail().isError()).matches(BOOLEAN_REGEX_MATCH));
        Assert.assertTrue(MESSAGE_IS_NOT_STRING, errorHolder.getDetail().getMessage().matches(GENERIC_STRING_REGEX));
        Assert.assertTrue(REQUESTED_ITEM_NOT_STRING, errorHolder.getDetail().getRequestedItem().matches(GENERIC_STRING_REGEX));
        Assert.assertTrue(SERVED_BY_IS_NOT_URL, errorHolder.getDetail().getServedBy().matches(URL_REGEX_MATCH));
    }

    @Then("^the user receives error ([^\"]*) failing to get unauthorized access to resources$")
    public void theUserReceivesErrorFailingToGetUnauthorizedAccessToResources(String errormessage) {
        LOGGER.info("Checking for unauthenticated state error.");
        Assert.assertEquals(UNAUTHORIZED_ERROR_NOT_PRESENT, response.path("detail"), errormessage);
    }

    @Given("the user calls the given endpoint with no route defined")
    public void theUserCallsTheGivenEndpointWithNoRouteDefined() {
        LOGGER.info(CALLING_THE_ENDPOINT + NO_ROUTE_DEFINED);
        get(GET_PRODUCTS_ENDPOINT);
    }
}




