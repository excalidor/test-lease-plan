package starter.Utils;

/**
 * String holder class
 */
public class Stringz {

    public static final String GET_PRODUCTS_ENDPOINT = "https://waarkoop-server.herokuapp.com/api/v1/search/demo";
    public static final String NO_REFERENCE_TO_PRODUCT = "The response does not contain any reference to product ";
    public static final String RESULT_NOT_EMPTY = "Result should be empty";
    public static final String RESULT_IS_EMPTY = "Result is empty!";
    public static final String NO_ERROR_MESSAGE = "No error message is present!";
    public static final String URL_REGEX_MATCH = "^(ht|f)tp(s?)\\:\\/\\/[0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*(:(0-9)*)*(\\/?)([a-zA-Z0-9\\-\\.\\?\\,\\'\\/\\\\\\+&amp;%\\$#_]*)?$";
    public static final String PRODUCTS_NOT_PRESENT = "Products not present!";
    public static final String PRODUCTS_ARE_PRESENT = "Products list is not empty!";
    public static final String GENERIC_ERROR_NOT_PRESENT = "Generic error not received.";
    public static final String UNAUTHORIZED_ERROR_NOT_PRESENT = "Not authenticated error failed to be received.";
    public static final String IMAGE_URL_ERROR = "Image address is not a valid URL.";
    public static final String TITLE_ERROR = "Title is not a valid string.";
    public static final String IS_PROMO_ERROR = "isPromo is not of boolean type.";
    public static final String PRICE_ERROR = "Price is not a double value.";
    public static final String BRAND_ERROR = "Brand is not a valid string.";
    public static final String PROMO_DETAILS__ERROR = "Promo details is not a valid string.";
    public static final String PROVIDER_ERROR = "Provider is not a valid string.";
    public static final String UNIT_ERROR = "Unit is not a valid string.";
    public static final String GENERIC_STRING_REGEX = "^.*$";
    public static final String BOOLEAN_REGEX_MATCH = "true|false";
    public static final String DOUBLE_REGEEX_MATCH = "^-?(\\d+(\\.\\d*)?|\\.\\d+)([eE][-+]?\\d+)?$";
    public static final String URL_IS_NOT_VALID = "URL is not a valid one!";
    public static final String CALLING_THE_ENDPOINT = "Calling the endpoint:";
    public static final String NO_ROUTE_DEFINED = "with no route defined.";
    public static final String IS_ERROR_NOT_BOOLEAN = "IsError field is not of a boolean type.";
    public static final String MESSAGE_IS_NOT_STRING = "Message is not a valid string.";
    public static final String REQUESTED_ITEM_NOT_STRING = "Requested item is not a valid string.";
    public static final String SERVED_BY_IS_NOT_URL = "Served_by does not contain a valid URL.";
    public static final String GENERIC_ERROR_RECEIVED = "Generic error received.";
    public static final String CHECKING_PRODUCT_DATA_VALIDITY = "Checking the product data validity.";
    public static final String CHECKING_ERROR_FIELDS_VALIDITY = "Checking the error message fields. Detail: ";
    public static final String EMPTY_LIST_EXPECTED = "Empty list expected.";
    public static final String ERROR_MESSAGE_EXPECTED = "Error message expected.";
    public static final String STATUS_CODE_CHECKING = "Status code checking.";
    public static final String CHECK_TITLE_FIELD = "Checking the results. They should contain data about ";
    public static final String EXPECTED_EMPTY_LIST = "Expected empty list.";
    public static final String EXPECTED_NOT_EMPTY_LIST = "Expected list of products to be not empty.";
    public static final String PRODUCT_LIST_SIZE_CHECK = "Product list size check to be greater than zero.";
    public static final String ERROR_MESSAGE_CONTENT_CHECK = "Error message content check.";
    public static final String URL_FORMAT_CHECK = "Endpoint URL format validity check.";
}
