package starter.stepImplementations;

import starter.api.ApiLayer;
import starter.models.Product;
import starter.models.Products;

/**
 * Step implementations class - used by Cucumber+Serenity
 */
public class SearchStepImplementations extends ApiLayer {

    /**
     * Method used for logging the content of products object (collection of products)
     *
     * @param products the products object holder
     */
    public void logProducts(Products products) {
        if (properties.get("logProducts").equals("true")) {
            for (Product product : products.getProducts()) {
                product.printProduct();
                System.out.println("-----------");
            }
        }
    }
}
