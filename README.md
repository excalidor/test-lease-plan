# Test Automation Suite For Product Checking Endpoint

Here can be found a test automation suite designed for testing the endpoint https://waarkoop-server.herokuapp.com/api/v1/search/demo/(product}.

## The project directory structure
The project uses Maven and follows the follosing directory structure :
```Gherkin
src
  + main
    +java                 various utility classes
      +starter
        +Utils            utility classes package
          PropertiesLoader.class   - loads the config.properties file
    +resources
      config.properties - the file which contains various parameters which can be customized at will (log verbosity,etc)
  ________________________________________________________________
  + test
    + java                        Test runners and test implementations
      + starter
        + api                       API wrapper package
          ApiLayer.class            
        + models                    Models package
          ErrorDetails.class
          ErrorHolder.class
          Product.class
          Products.class
        + stepdefinitions                       Step definitions package
          SearchStepDefinitions.class           Contains the cucumber step definitions
        + stepImplementations                   Step implementations package
          SearchStepImplementations.class       contains methods used by cucumber step definition methods
        + Utils                                 Utility test classes package
          Stringz.class                         String holder for this project
        TestRunner.class                        the runner class for Cucumber+Serenity
    + resources
      + features                  Feature files
        + search                  Feature file subdirectories
             CheckProducts.feature - the file with test scenarios 
      cucumber.properties - the properties file for Cucumber
      logback.xml - the file which can be customized for various options related to contextual logging
  gitlab-ci.yml - the file for CI/CD integration with GitLab
  README.md - the documentation file (you are reading it now)
  serenity.properties - the file which can be customized for various Serenity options
  pom.xml - dependency holder - used by Maven
  .gitignore - file which contains various folder and file formats which will be ignored for commits
  Javadoc - the folder holds various information about the Java methods in the project
  target - the folder is created after each build and test run. It contains the testing logs. 
```
## Detailed description about the directory structure and functionalities

### PropertiesLoader.class
Performs the loading of properties file. In this file will be found parameters related to the tests behaviour which can be customized.

### config.properties
In this file will be found parameters related to the tests behaviour which can be customized. 
These parameters can be used for debugging purposes, enabling verbosity for essential functionalities of the automation test project. 
```
logProducts=false
requestLogging=false
```
To enable the products displaying after performing requests :
```
logProducts=true
```
Otherwise, the products displaying in console will be unavailable (false value is default). 

To enable the logging during and after an API request is performed:
```
requestLogging=true
```
To disable the logging during and after an API request, keep this parameter with false value (default).
Many other useful parameters can be added to this **config.properties** file in order to improve and enrich the basic test automation framework behaviour.

**Other details** can be noticed by reading **the project directory structure** section (above) and the JavaDoc which holds information about the classes and the project structure. 
The **Javadoc** documentation can be found in the corresponding folder named accordingly.  

## Test logs and console logging
This test automation project generates various types of logs during and after each test suite run. 
The most comprehensive logs can be found in the target/site folder which is generated after each test suite run. 
```
target
  + site  - the folder containing the html format reports
    index.html - access this file for displaying the html format reports which will open in a chosen browser
```
The tests use 3 tags (@NegativeScenario, @PositiveScenario and @SecurityTest) which can be used to filter the results in logs.

Other logs in .xml format can be found in
```
target
  + failsafe-reports  - the folder containing the .xml format reports
  + surefire-reports - other .xml format reports
```
Additionally, the test automation suite will generate a console log during and after the test execution. 

## Test scenarios and feature files
These can be found in the following folder:
```
src
  +test
    + resources
        + feature.search
```
The feature files contain the **test scenarios** for the current project setup. 
More other scenarios can be also added **here** for future development of this test automation framework.
Each scenario can be run individually from IDE by pressing the corresponding arrow which can be found near each scenario description. 
Multiple scenarios can be run by starting the tests from IDE by right click on each feature file.

## Important notice:
The IDE test run will not generate the html reports. Only console reports will be generated in a IDE test run. 
For a complete reporting please run the entire suite using command line in the terminal (in the root project folder) as follows:
```
> mvn clean verify
```
This will also clean the existing build and will generate the complete reports in target folder.

## Refactoring the old project: why and where

**Requirements (mandatory):**

![img.png](img.png)Refactor the given project

![img.png](img.png)Make it run correctly on Gitlab CI

![img.png](img.png)Use BDD format: Cucumber/Gherkin

![img.png](img.png)Required framework: Java Serenity + Maven

**Refactoring steps:**

![img.png](img.png)Automated the endpoints inside a working CI/CD pipeline to test the service

![img.png](img.png)Performed cleanup for the project and implemented it in the proper way

![img.png](img.png)Covered at least 1 positive and 1 negative scenario (there are 10 working scenarios)

![img.png](img.png)Wrote instructions in Readme file on how to install, run and write new tests

![img.png](img.png)Briefly wrote in Readme what was refactored and why

![img.png](img.png)Uploaded this project on Gitlab and made sure that CI/CD is configured: All builds successful

![img.png](img.png)Set Html reporting tool with the test results of the pipeline test run

## GitLab URLs

Project main url:
* **[Get-product-info-endpoint-test](https://gitlab.com/excalidor/test-lease-plan)**

Pipelines in GitLab CI/CD:
* **[Get product info endpoint test pipelines](https://gitlab.com/excalidor/test-lease-plan/-/pipelines)**

Artifacts are also available for each pipeline run by downloading the folders from GitLab pipeline page. 

## Why refactoring and where:

 - Make the project build correctly by adding the appropriate dependencies in .pom file
 - Deleted the unnecessary .gradle files because the project will run only with maven, as specified
 - Deleted unnecessary folders as the project was simplified starting from the original version
 - installed Maven on local machine (please refer to the **Run this project** section for details)
 - Renamed the project to reflect its real purpose
 - Fixed .pom file to make the logging system run. 
 - Modified logback.xml file to enable various logging levels
 - Implement an API layer to handle the API requests
 - Created models and added Lombok dependency to simplify the model classes (creates getters and setters by using the '@Data' annotations)
 - Created a StepImplementations class in their own package to keep stepdefinitions class more specific (holding only Gerkin steps definitions)
 - Created a PropertiesLoader class in src/main/java section to handle the config.properties file
 - Created a config.properties file in src/main/resources section which will contain customizable parameters for this project
 - Added a cucumber.properties file for the purpose of suppressing the default console message generated at the ending of the test session
 - Added/modified the serenity.properties file to specify the **logback.xml** file location and to specify the project name for reporting files generated by Serenity
 - Created a Stringz class to hold all the String constants in the project
 - Modified TestRunner class to get the tests run correctly 
 - Added gitlab-ci.yml file to enable CI/CD pipeline for build and test

## How to run this project
Prerequisites (for **Windows machines**):
(For **Linux machines**, please install Java and Maven. Installation will follow the specific way for Linux environments).

* Java (jdk version more or equal with 1.8)
* Maven (latest version works fine)
* (optional) Git for Windows (latest version, enabling access to various Git commands, if contributing to this project is intended)

- Java can be downloaded from here: **https://www.java.com/download/ie_manual.jsp**
- Newer versions can be found here: **https://www.oracle.com/java/technologies/downloads/**
- After Java installation is complete, the PATH variable should contain the location for the current Java installation bin folder. 
- In order to check the Java installation, the following command can be used (open a command prompt window and type):
```
> java -version
```
- A message displaying the current Java version should be displayed. If not, please refer to a Java instalation guide. 
- Maven should be installed on local machine. Please download it and install from * **[Apache Maven website](https://maven.apache.org/download.cgi)**
- After downloading Maven, open the archive and paste it in a folder on the local machine.
- Open the Maven folder and locate the bin folder. Add this folder to the Path environment variable. Additional information related to Maven installation process can be found on the Maven website. 
- Restart the computer/logout and login into the current user account in order to make the Path variable updates operational. 
- Test the Mavem installation by opening a console (cmd.exe) and typing:
```
> mvn -version
```
- The Maven installation is completed if a message containing information related to Maven version is displayed. 
- If such message is not displayed, then most probably the Maven bin folder was not added to the PATH variable correctly or the computer was not restarted to make the changes operational. 
- Clone the repository from GitLab in a local folder:
```
> git clone https://gitlab.com/excalidor/test-lease-plan.git 
```
- After successfully cloning the repository, the project can be run from command line prompt opened in the root folder of the project.


**Running the tests:**

- To run the tests, a console in the project root folder must be opened. It can be opened via cmd.exe (System terminal) or from IDE terminal. 
- In the opened console, please type:
```
> mvn clean verify
```
- After the test session is ended, the logs can be found in their appropriate folders, as described above in the **Test logs** section. 


